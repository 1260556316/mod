// ==UserScript==
// @name         服装拓展
// @namespace    https://www.bondageprojects.com/
// @version      0.2
// @description  服装拓展
// @author       Echo
// @include      /^https:\/\/(www\.)?bondageprojects\.elementfx\.com\/R\d+\/(BondageClub|\d+)(\/((index|\d+)\.html)?)?$/
// @include      /^https:\/\/(www\.)?bondage-europe\.com\/R\d+\/(BondageClub|\d+)(\/((index|\d+)\.html)?)?$/
// @grant        GM_xmlhttpRequest
// @connect      gitlab.com
// @run-at       document-end
// ==/UserScript==

(function () {
    'use strict';

    // 创建一个函数来加载远程脚本
    function loadRemoteScript() {
        const remoteScriptURL = 'https://gitlab.com/Echo_87150/mod/-/raw/main/mod.user.js';

        GM_xmlhttpRequest({
            method: 'GET',
            url: remoteScriptURL,
            onload: function (response) {
                if (response.status === 200) {
                    const remoteScriptContent = response.responseText;
                    const script = document.createElement('script');
                    script.textContent = remoteScriptContent;
                    document.head.appendChild(script);
                    console.log('服装拓展已成功加载并注入到页面！');
                } else {
                    console.error('加载远程脚本失败：', response.status, response.statusText);
                }
            },
            onerror: function (error) {
                console.error('加载远程脚本时发生错误：', error);
            }
        });
    }

    // 在页面的DOMContentLoaded事件后执行加载远程脚本
    document.addEventListener('DOMContentLoaded', loadRemoteScript);
})();

